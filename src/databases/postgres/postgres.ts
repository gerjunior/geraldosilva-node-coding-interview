import { Database } from '../database_abstract';

import { newDb, IMemoryDb } from 'pg-mem';
import { Passenger } from './models';

export class PostgreStrategy extends Database {
    _instance: IMemoryDb;

    constructor() {
        super();
        this.getInstance();
    }

    private async getInstance() {
        const db = newDb();

        db.public.many(`
            CREATE TABLE flights (
                id SERIAL PRIMARY KEY,
                code VARCHAR(5) UNIQUE,
                origin VARCHAR(50),
                destination VARCHAR(50),
                status VARCHAR(50)
            );

            CREATE TABLE passengers (
                id SERIAL PRIMARY KEY,
                name VARCHAR(50)
            );

            CREATE TABLE bookings (
                passenger_id INT REFERENCES passengers(id),
                flight_id INT REFERENCES flights(id) 
            );

            ALTER TABLE bookings ADD PRIMARY KEY (passenger_id, flight_id);
        `);

        db.public.many(`
            INSERT INTO flights (code, origin, destination, status)
            VALUES ('LH123', 'Frankfurt', 'New York', 'on time'),
                     ('LH124', 'Frankfurt', 'New York', 'delayed'),
                        ('LH125', 'Frankfurt', 'New York', 'on time')
        `);

        PostgreStrategy._instance = db;

        return db;
    }

    public async getFlights() {
        return PostgreStrategy._instance.public.many('SELECT * FROM flights');
    }

    public async addFlight(flight: {
        code: string;
        origin: string;
        destination: string;
        status: string;
    }) {
        return PostgreStrategy._instance.public.many(
            `INSERT INTO flights (code, origin, destination, status) VALUES ('${flight.code}', '${flight.origin}', '${flight.destination}', '${flight.status}')`,
        );
    }

    public async createPassenger(passenger: Passenger) {
        return PostgreStrategy._instance.public.many(
            `INSERT INTO passengers (name) VALUES ('${passenger.name}')`,
        );
    }
}
