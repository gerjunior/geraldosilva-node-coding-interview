export interface Flight {
    code: string;
    origin: string;
    destination: string;
    status: string;
}

export interface Passenger {
    name: string;
}

export interface Booking {
    passenger_id: string;
    flight_id: string;
}
