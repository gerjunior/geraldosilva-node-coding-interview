import { Flight, Passenger } from './postgres/models';

export class Database {
    public static _instance: any;

    public static getInstance() {
        // subclass must implement this method
    }

    public async getFlights() {
        // subclass must implement this method
    }

    public async updateFlightStatus(code: string) {
        // subclass must implement this method
    }

    public async addFlight(flight: Flight) {
        // subclass must implement this method
    }

    public async createPassenger(passenger: Passenger) {
        // subclass must implement this method
    }
}
