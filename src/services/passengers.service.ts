import { Database } from '../databases/database_abstract';
import { DatabaseInstanceStrategy } from '../database';
import { Flight, Passenger } from '../databases/postgres/models';

export class PassengersService {
    private readonly _db: Database;

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance();
    }

    public async createPassenger(passenger: Passenger) {
        return this._db.createPassenger(passenger);
    }
}
