import { JsonController, Post, Body } from 'routing-controllers';
import { PassengersService } from '../services/passengers.service';
import { Passenger } from '../databases/postgres/models';

@JsonController('/passengers', { transformResponse: false })
export default class PassengersController {
    private _passengersService: PassengersService;

    constructor() {
        this._passengersService = new PassengersService();
    }

    @Post('')
    async createPassenger(
        @Body()
        passenger: Passenger,
    ) {
        return {
            status: 200,
            data: await this._passengersService.createPassenger(passenger),
        };
    }
}
